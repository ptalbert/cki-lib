"""YAML utils."""
import copy
import os
import pathlib
import typing

import yaml

from . import config_tree
from .session import get_session

SESSION = get_session(__name__, raise_for_status=True)


# pylint: disable=too-many-ancestors
class BlockDumper(yaml.SafeDumper):
    """Block-style literals for strings with newlines.

    This can be used via
        from cki_lib.yaml import BlockDumper
        yaml.dump(what, Dumper=BlockDumper, **kwargs)
    """

    def represent_str(self, data: str) -> yaml.ScalarNode:
        """Format strings with newlines as block-style literals."""
        if '\n' not in data:
            return super().represent_str(data)
        return self.represent_scalar('tag:yaml.org,2002:str', data, style='|')


class ReferenceList(typing.List[typing.Any]):
    """Holds references like !reference [path, to, node]."""

    @staticmethod
    def find_reference(path: 'ReferenceList', root: typing.Any = None) -> typing.Any:
        """Find the node belonging to a !reference [path, to, node]."""
        if not isinstance(root, dict):
            raise Exception('Root node is not a dictionary')
        current = root
        for step in path:
            current = current[step]
        return copy.deepcopy(current)

    @staticmethod
    def resolve_references(data: typing.Any, root: typing.Any = None) -> typing.Any:
        """Resolve references like !reference [path, to, node]."""
        if root is None:
            root = data
        if isinstance(data, dict):
            for key, value in data.items():
                if isinstance(value, ReferenceList):
                    data[key] = ReferenceList.find_reference(value, root)
                else:
                    data[key] = ReferenceList.resolve_references(value, root)
        elif isinstance(data, list):
            for key, value in enumerate(data):
                if isinstance(value, ReferenceList):
                    data[key] = ReferenceList.find_reference(value, root)
                else:
                    data[key] = ReferenceList.resolve_references(value, root)
        return data


class ReferenceLoader(yaml.SafeLoader):
    """Resolve references like !reference [path, to, node]."""

    def construct_reference(self, node: yaml.nodes.Node) -> ReferenceList:
        """Construct a list marked as a reference list."""
        return ReferenceList(self.construct_sequence(node))  # type: ignore


yaml.add_representer(str, BlockDumper.represent_str, Dumper=BlockDumper)
yaml.add_constructor('!reference', ReferenceLoader.construct_reference, Loader=ReferenceLoader)


def _resolve_includes(
        data: typing.Dict[str, typing.Any],
        relative_to: typing.Union[pathlib.Path, str, None],
) -> typing.Dict[str, typing.Any]:
    includes = data.pop('.include', None)
    if includes is None:
        return data
    if isinstance(includes, str):
        includes = [includes]
    if not isinstance(includes, list):
        raise Exception('.include is not a string or list')
    root_directory = os.path.dirname(relative_to) if relative_to else os.getcwd()
    result: typing.Dict[typing.Any, typing.Any] = {}
    for include in includes:
        if include.startswith('https://') or include.startswith('http://'):
            config_tree.merge_dicts(result, load(contents=SESSION.get(include).text))
        else:
            config_tree.merge_dicts(result, load(file_path=f'{root_directory}/{include}'))
    config_tree.merge_dicts(result, data)
    return result


def load(*,
         file_path: typing.Union[pathlib.Path, str, None] = None,
         contents: typing.Optional[str] = None,
         resolve_references: bool = False,
         resolve_includes: bool = False,
         ) -> typing.Any:
    """Load a YAML file.

    resolve_references: support !reference tag as described at
        <https://docs.gitlab.com/ee/ci/yaml/#reference-tags>
    resolve_includes: support one level of .include: <url|file_name>
    """
    if contents is None and file_path:
        contents = pathlib.Path(file_path).read_text(encoding='utf8')
    if contents is None:
        return None
    result = yaml.load(contents, Loader=ReferenceLoader)
    if resolve_includes and isinstance(result, dict):
        result = _resolve_includes(result, relative_to=file_path)
    if resolve_references:
        result = ReferenceList.resolve_references(result)
    return result
