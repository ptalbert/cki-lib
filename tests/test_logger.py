"""logger tester."""
import io
import logging
import os
import unittest
from unittest import mock

from cki_lib import logger

MOCK_STDERR = io.StringIO()


class TestLogger(unittest.TestCase):
    """Test logger class."""

    def test_cki_prepended(self):
        """test cki prefix added to name if missing from passed name"""
        mod_a_logger = logger.get_logger('mod_a')
        self.assertEqual(mod_a_logger.name, 'cki.mod_a')

    def test_truncate_logs(self):
        """Ensure truncate_logs works."""
        test_data = 'eat a hotdog'
        dst_file = 'unittesting.log'

        with open(os.path.join(logger.CKI_LOGS_FILEDIR, dst_file),
                  'w') as fobj:
            fobj.write(test_data)

        # empty all logfiles + check logfile we just changed is now empty
        logger.truncate_logs()
        with open(os.path.join(logger.CKI_LOGS_FILEDIR, dst_file),
                  'r') as fobj:
            self.assertEqual(fobj.read(), '')


@mock.patch('cki_lib.logger.STREAM', new=MOCK_STDERR)
class TestLoggerPatched(unittest.TestCase):
    """Test logger class for methods that need STREAM patched."""

    @classmethod
    def reset_logger(cls):
        """remove all handlers from cki logger to allow simulating initial
        creation"""
        logging.getLogger().handlers = []
        logger.CKI_HANDLER = False
        MOCK_STDERR.truncate(0)

    def test_propagation(self):
        """test inheritance and propagation"""
        cki_logger = logger.get_logger('cki')
        mod_a_logger = logger.get_logger('cki.mod_a')
        mod_b_logger = logger.get_logger('cki.mod_b')

        # root will default to logging.WARNING
        cki_logger.setLevel(logging.WARNING)
        mod_a_logger.setLevel(logging.CRITICAL)
        mod_b_logger.setLevel(logging.DEBUG)

        # cki logs warning but not DEBUG
        cki_logger.warning('test_propagation')
        self.assertTrue(' - [WARNING] - cki - test_propagation'
                        in MOCK_STDERR.getvalue())
        MOCK_STDERR.truncate(0)
        cki_logger.debug('cki_logger')
        self.assertEqual(MOCK_STDERR.getvalue(), '')
        MOCK_STDERR.truncate(0)

        # mod_a doesn't log WARNING
        mod_a_logger.warning('mod_a_logger')
        self.assertEqual(MOCK_STDERR.getvalue(), '')
        MOCK_STDERR.truncate(0)

        # mod_b logs DEBUG
        mod_b_logger.debug('mod_b_logger')
        self.assertTrue(' - [DEBUG] - cki.mod_b - mod_b_logger'
                        in MOCK_STDERR.getvalue())

    def test_handlers(self):
        """test only one handler is created"""
        self.reset_logger()

        logger1 = logger.get_logger('logger1')
        logger.get_logger('logger2')
        logger1.setLevel(logging.WARNING)
        logger1.warning('logger1')
        self.assertTrue(MOCK_STDERR.getvalue().count('\n') == 1)

    def test_env_config(self):
        """test default logging level and that CKI_LOGGING_LEVEL changes it"""
        self.reset_logger()

        # defaults to WARNING
        cki_logger = logger.get_logger('cki')
        cki_logger.warning('test_env_config')
        self.assertTrue(' - [WARNING] - cki - test_env_config'
                        in MOCK_STDERR.getvalue())
        MOCK_STDERR.truncate(0)
        cki_logger.info('test_env_config')
        self.assertEqual(MOCK_STDERR.getvalue(), '')

        # CKI_LOGGING_LEVEL changes level if cki logger recreated
        self.reset_logger()
        with mock.patch.dict(os.environ, {'CKI_LOGGING_LEVEL': 'INFO'}):
            cki_logger = logger.get_logger('cki')
        cki_logger.info('test_env_config')
        self.assertTrue(' - [INFO] - cki - test_env_config'
                        in MOCK_STDERR.getvalue())
