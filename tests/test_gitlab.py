"""Gitlab api interaction tests."""
import datetime
import unittest
from unittest import mock

from dateutil.parser import parse as date_parse
from gitlab import Gitlab
from gitlab.v4 import objects as gitlab_objects
import responses

from cki_lib import gitlab


class PipelineMock:
    # pylint: disable=too-few-public-methods, redefined-builtin, invalid-name
    """PipelineMock."""

    def __init__(self, created_at, id):
        """Initialization."""
        self.created_at = created_at
        self.id = id


class MockedGet:
    """Mock for requests.get."""

    def __init__(self, status_code=200, returns_json=True):
        """Init."""
        self.status_code = status_code
        self.returns_json = returns_json

    def __call__(self, url, **kwds):
        """Fake __call__."""
        return self

    def json(self):
        """Fake json()."""
        if self.returns_json:
            return {}
        raise ValueError

    @property
    def content(self):
        """Fake content."""
        return b''


class TestGitlabHelper(unittest.TestCase):
    """Test Gitlab class."""

    @mock.patch('gitlab.Gitlab', mock.Mock())
    def setUp(self):
        """SetUp class."""
        self.gitlab = gitlab.GitlabHelper('url', 'token')

    def test_set_project(self):
        """Test set_project sets the correct value."""
        mocked_gitlab = mock.Mock()
        self.gitlab.api.projects.get.return_value = mocked_gitlab

        self.gitlab.set_project(1)
        self.assertEqual(self.gitlab.project, mocked_gitlab)

    def test_set_project_call(self):
        """Test set_project calls _get_project"""
        mocked_gitlab = mock.Mock()
        self.gitlab._get_project = mocked_gitlab  # pylint: disable=protected-access # noqa

        self.gitlab.set_project(2)
        mocked_gitlab.assert_called_with(2)

    @mock.patch('requests.get')
    def test_get_artifact(self, mocked_requests):
        """Test get_artifact."""
        mocked_requests.side_effect = mock.Mock()
        self.gitlab.project = mock.Mock()
        self.gitlab.project.id = 'mocked_id'

        self.gitlab.get_artifact(99, 'file.name')

        mocked_requests.assert_has_calls([
            mock.call(
                'url/api/v4/projects/mocked_id/jobs/99/artifacts/file.name',
                # noqa
                headers={'Private-Token': 'token'}),
        ])

    @mock.patch('requests.get')
    def test_get_artifact_404(self, mocked_requests):
        """Test get_artifact."""
        mocked_requests.side_effect = MockedGet(status_code=404)
        self.gitlab.project = mock.Mock()
        self.gitlab.project.id = 'mocked_id'

        self.assertEqual(
            None,
            self.gitlab.get_artifact(99, 'file.name')
        )

    @mock.patch('requests.get')
    def test_get_artifact_type(self, mocked_requests):
        """Test get_artifact return type."""
        self.gitlab.project = mock.Mock()
        self.gitlab.project.id = 'mocked_id'

        mocked_requests.side_effect = MockedGet(returns_json=True)
        self.assertEqual(
            dict,
            type(self.gitlab.get_artifact(99, 'file.name'))
        )

        mocked_requests.side_effect = MockedGet(returns_json=False)
        self.assertEqual(
            bytes,
            type(self.gitlab.get_artifact(99, 'file.name'))
        )

        mocked_requests.side_effect = MockedGet(returns_json=True)
        self.assertEqual(
            bytes,
            type(self.gitlab.get_artifact(99, 'file.name', raw=True))
        )

    def test_get_pipelines_since(self):
        """Test get_pipelines_since."""
        self.gitlab.project = mock.Mock()
        self.gitlab.requests_per_page = 1

        pipes = [
            PipelineMock("2019-09-29T00:00:00.00000Z", 5),
            PipelineMock("2019-09-28T00:00:00.00000Z", 4),
            PipelineMock("2019-09-27T00:00:00.00000Z", 3),
            PipelineMock("2019-09-26T00:00:00.00000Z", 2),
            PipelineMock("2019-09-25T00:00:00.00000Z", 1),
        ]

        def get_pipe(get_id):
            for pipe in pipes:
                if pipe.id == get_id:
                    return pipe
            return None

        self.gitlab.project.pipelines.get.side_effect = get_pipe

        def list_pipe(per_page, page):
            return pipes[page - 1:page - 1 + per_page]

        self.gitlab.project.pipelines.list.side_effect = list_pipe

        returned = self.gitlab.get_pipelines_since(
            date_parse("2019-09-27T00:00:01.000000Z")
        )

        self.gitlab.project.pipelines.get.assert_has_calls([
            mock.call(5),
            mock.call(4),
            mock.call(3),
        ])

        self.assertEqual(returned, pipes[:3])

        # test naive format
        returned = self.gitlab.get_pipelines_since(
            datetime.datetime.strptime("2019-09-27T00:00:01.00000Z",
                                       gitlab.GITLAB_TIMESTAMP_FORMAT))

        self.assertEqual(returned, pipes[:3])


class TestGitLab(unittest.TestCase):
    """Test GitLab helpers."""

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    @mock.patch('cki_lib.gitlab.get_token')
    def test_get_instance_get_token(get_token):
        """Check that get_token is used."""
        gitlab.get_instance('https://a')
        get_token.assert_called()

    @staticmethod
    @mock.patch.dict('os.environ', {'GITLAB_TOKENS': '{}'})
    @mock.patch('cki_lib.gitlab.get_token')
    def test_get_instance_no_get_token(get_token):
        """Check that get_token is not used if token is provided."""
        gitlab.get_instance('https://a', token='c')
        get_token.assert_not_called()

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{}'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_no_token(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token=None)

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{"a":"b"}'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_no_token_var(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token=None)

    @staticmethod
    @mock.patch.dict('os.environ', {'TEST_TOKENS': '{"a":"b"}', 'b': 'c'})
    @mock.patch('gitlab.Gitlab')
    def test_get_instance_custom_tokens(mock_gitlab):
        """Check that the provided env name is used."""
        gitlab.get_instance('https://a', env_name='TEST_TOKENS')
        mock_gitlab.assert_called_with(
            'https://a', session=mock.ANY, private_token='c')


class TestGitLabParseURL(unittest.TestCase):
    """Test parse_gitlab_url."""

    @staticmethod
    def mock_responses():
        """Set up tests."""
        url = 'https://gitlab.com/api/v4/projects'
        mocks = [
            ('/group_foo%2Fproject_foo', {'id': 1}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo', {'id': 2}),
            ('/group_foo%2Fproject_foo/pipelines/1234', {'id': 1234}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/pipelines/1234', {'id': 1234}),
            ('/group_foo%2Fproject_foo/merge_requests/4321', {'id': 4321, 'iid': 12}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/merge_requests/4321',
             {'id': 4321, 'iid': 12}),
            ('/group_foo%2Fproject_foo/merge_requests/12/notes/1122', {'id': 1122}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/merge_requests/12/notes/1122',
             {'id': 1122}),
            ('/group_foo%2Fproject_foo/pipeline_schedules/9', {'id': 1}),
            ('/group_foo%2Fproject_foo%2Fsubproject_foo/pipeline_schedules/9', {'id': 2}),
        ]

        for endpoint, payload in mocks:
            responses.add(responses.GET, url + endpoint, json=payload)

    @responses.activate
    def test_get_pipeline(self):
        """Test parse_gitlab_url with pipeline urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/pipelines/1234',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/pipelines/1234',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectPipeline)
            self.assertEqual(1234, obj.id)

    @responses.activate
    def test_get_merge_request(self):
        """Test parse_gitlab_url with MR urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/merge_requests/4321',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/merge_requests/4321',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectMergeRequest)
            self.assertEqual(4321, obj.id)

    @responses.activate
    def test_get_merge_request_note(self):
        """Test parse_gitlab_url with MR Note urls."""
        self.mock_responses()
        urls = [
            'https://gitlab.com/group_foo/project_foo/-/merge_requests/4321#note_1122',
            'https://gitlab.com/group_foo/project_foo/subproject_foo/-/merge_requests/4321'
            '#note_1122',
        ]
        for url in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectMergeRequestNote)
            self.assertEqual(1122, obj.id)

    @responses.activate
    def test_get_project(self):
        """Test parse_gitlab_url with Project urls."""
        self.mock_responses()
        urls = [
            ('https://gitlab.com/group_foo/project_foo', 1),
            ('https://gitlab.com/group_foo/project_foo/subproject_foo', 2),
        ]
        for url, id in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.Project)
            self.assertEqual(id, obj.id)

    @responses.activate
    def test_get_schedule(self):
        """Test parse_gitlab_url with pipeline schedule urls."""
        self.mock_responses()
        urls = [
            ('https://gitlab.com/group_foo/project_foo/-/pipeline_schedules/9', 1),
            ('https://gitlab.com/group_foo/project_foo/subproject_foo/-/pipeline_schedules/9', 2),
        ]
        for url, id in urls:
            gl_instance, obj = gitlab.parse_gitlab_url(url)

            self.assertIsInstance(gl_instance, Gitlab)
            self.assertIsInstance(obj, gitlab_objects.ProjectPipelineSchedule)
            self.assertEqual(id, obj.id)
