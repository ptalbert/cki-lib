"""Patchwork api interaction tests."""
import unittest
from unittest import mock

from freezegun import freeze_time
from furl import furl

from cki_lib import patchwork


class MockedGet:
    """Mock for requests.get."""

    def __init__(self, max_pages=None):
        """Init."""
        self.max_pages = max_pages
        self.call_page = None

    def __call__(self, url):
        """Fake __call__."""
        url = furl(url)
        self.call_page = int(url.args.get('page', 1))

        return self

    @staticmethod
    def json():
        """Fake json()."""
        return {}

    @property
    def status_code(self):
        """Fake status_code."""
        if self.max_pages and self.call_page > self.max_pages:
            return 404

        return 200


class TestPatchwork(unittest.TestCase):
    """Test Patchwork class."""

    def setUp(self):
        """SetUp class."""
        self.url = 'http://url'
        self.patchwork = patchwork.Patchwork(self.url)

    @mock.patch('requests.get')
    def test_get_patch(self, mocked_requests):
        """Test retreiving a single patch."""
        mocked_requests.side_effect = MockedGet()

        self.patchwork.get_patch(1)

        mocked_requests.assert_called_with(f'{self.url}/patches/1')

    def test_get_patch_from_url(self):
        """Test get_patch_from_url fetches the correct id."""
        self.patchwork.get_patch = mock.Mock()

        testcases = [
            ('1570143', 'http://patchwork-url.com/patch/1570143/mbox/'),
            ('1570144', 'http://patchwork-url.com/patch/1570144'),
            ('1570145', 'http://patchwork-url.com/patch/1570145/')]

        for patch_id, url in testcases:
            self.patchwork.get_patch_from_url(url)
            self.patchwork.get_patch.assert_called_with(patch_id)

    @mock.patch('requests.get')
    def test_get_paginated_raises(self, mocked_requests):
        """Test _get_paginated function honors max_pages."""
        self.patchwork.max_pages = 1
        mocked_requests.side_effect = MockedGet(max_pages=3)

        self.assertRaises(
            patchwork.MaxIterationsReached,
            self.patchwork._get_paginated,  # pylint: disable=protected-access
            'url')

    @mock.patch('requests.get')
    def test_get_paginated(self, mocked_requests):
        """Test _get_paginated function."""
        mocked_requests.side_effect = MockedGet(max_pages=3)

        self.patchwork._get_paginated(self.url)  # pylint: disable=protected-access # noqa

        mocked_requests.assert_has_calls([
            mock.call(f'{self.url}?page=1'),
            mock.call(f'{self.url}?page=2'),
            mock.call(f'{self.url}?page=3'),
            mock.call(f'{self.url}?page=4'),  # Last one is 404
        ])

    @mock.patch('cki_lib.patchwork.Patchwork._get_paginated')
    @freeze_time("2010-01-02 09:00:00", tz_offset=1)
    def test_get_patches_since(self, get_paginated):
        """Test getting patches since date - since_days."""

        self.patchwork.get_patches_since('project', since_days=1)
        get_paginated.assert_called_with(
            f'{self.url}/patches?project=project&since='
            f'2010-01-01T10:00:00+00:00'
        )

    @mock.patch('requests.get')
    def test_get_series(self, mocked_requests):
        """Test retreiving the patches from a series."""

        mocked_requests.side_effect = MockedGet()

        self.patchwork.get_series(1)

        mocked_requests.assert_called_with(f'{self.url}/series/1')

    @mock.patch('cki_lib.patchwork.Patchwork._get_paginated')
    @freeze_time("2010-01-02 09:00:00", tz_offset=1)
    def test_get_series_since(self, get_paginated):
        """Test getting series since date - since_days."""
        get_paginated.return_value = mock.MagicMock()

        self.patchwork.get_series_since('project', since_days=1)
        get_paginated.assert_called_with(
            f'{self.url}/series?project=project&since=2010-01-01T10:00:00'
        )
