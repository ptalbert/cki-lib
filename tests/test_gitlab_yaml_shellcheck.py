"""Test gitlab-yaml-shellcheck."""
import contextlib
import io
import pathlib
import subprocess
import sys
import tempfile
import typing
import unittest
from unittest import mock

from cki_lib import gitlab_yaml_shellcheck


class TestShellCheck(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki_lib/gitlab_yaml_shellcheck.py."""

    @staticmethod
    @contextlib.contextmanager
    def redirect(what: str, new: io.StringIO) -> typing.Iterator[io.StringIO]:
        """Redirect a stream."""
        old = getattr(sys, what)
        setattr(sys, what, new)
        if what == 'stdin':
            new.name = '<stdin>'
        try:
            yield new
        finally:
            setattr(sys, what, old)

    @staticmethod
    def _run(*,
             args: typing.Optional[typing.Iterable[str]] = None,
             cache: bool = False,
             files: typing.Optional[typing.Mapping[str, typing.List[str]]] = None,
             stdin: typing.Optional[typing.List[str]] = None,
             filename: str = 'main.yml',
             ) -> typing.Tuple[int, typing.List[typing.List[str]], str]:

        with tempfile.TemporaryDirectory() as directory:
            for name, contents in (files or {}).items():
                path = pathlib.Path(directory, name)
                path.parent.mkdir(parents=True, exist_ok=True)
                path.write_text('\n'.join(contents), encoding='utf8')
            with TestShellCheck.redirect('stdout', io.StringIO()) as stdout,\
                    TestShellCheck.redirect('stdin', io.StringIO('\n'.join(stdin or []))):
                list_args = list(a.format(directory=directory) for a in args or [])
                if not cache:
                    list_args.append('--no-cache')
                if stdin:
                    list_args += ['-']
                else:
                    list_args += [f'{directory}/{filename}']
                code = gitlab_yaml_shellcheck.main(list_args)
            output = stdout.getvalue().rstrip()
            return code, [line.split(':', 3) for line in
                          output.split('\n')], output

    def _assert_messages(self,
                         code: int,
                         messages: typing.List[typing.List[str]],
                         expected: typing.List[typing.Tuple[str, int, str]]
                         ) -> None:
        if not expected:
            self.assertEqual(code, 0)
            return
        for expected_filename, expected_line, expected_sc in expected:
            try:
                matches = [m for m in messages
                           if m[0].endswith(expected_filename) and m[1] == str(expected_line)]
                self.assertEqual(len(matches), 1)
                self.assertIn(expected_sc, matches[0][3])
            except Exception:
                print(messages)
                raise

    def test_line_numbers(self) -> None:
        """Test the line number mapping."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
        ])

    def test_multiple_jobs(self) -> None:
        """Test the merging of multiple jobs."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'job1:',
                    '  script:',
                    '    - unused1=1',
                    'job2:',
                    '  script:',
                    '    - unused2=1',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
            ('main.yml', 6, 'SC2034'),
        ])

    def test_global_before_script(self) -> None:
        """Test the merging of global scripts."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'before_script:',
                    '  - unused1=1',
                    'job:',
                    '  script:',
                    '    - unused=1',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main.yml', 2, 'SC2034'),
            ('main.yml', 5, 'SC2034'),
        ])

    def test_extends(self) -> None:
        """Test the merging of extended jobs."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    '.job:',
                    '  before_script:',
                    '    - unused1=1',
                    'job:',
                    '  extends: .job',
                    '  script:',
                    '    - unused2=1',
                ],
            },
            args=['--job', 'job'],
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
            ('main.yml', 7, 'SC2034'),
        ])

    def test_extends_multiple(self) -> None:
        """Test the merging of jobs with multiple extends."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    '.job1:',
                    '  before_script:',
                    '    - unused1=1',
                    '.job2:',
                    '  script:',
                    '    - unused2=1',
                    'job:',
                    '  extends: [.job1, .job2]',
                ],
            },
            args=['--job', 'job'],
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
            ('main.yml', 6, 'SC2034'),
        ])

    def test_variables(self) -> None:
        """Test the definition of variables."""

        _, _, output = self._run(
            files={
                'main.yml': [
                    'variables:',
                    '  var3: baz',
                    'job:',
                    '  variables:',
                    '    var1: baz',
                    '  script:',
                    '    - var2=$var1',
                    '    - echo "$var2"',
                    '    - echo "$var3"',
                ],
            },
        )
        self.assertNotIn('SC2154', output)

    def test_predefined_variables(self) -> None:
        """Test the predefined variables."""

        _, _, output = self._run(
            files={
                'main.yml': [
                    'job:',
                    '  script:',
                    '    - var2=$var1',
                    '    - echo "$var2"',
                    '    - echo "$var3"',
                ],
            },
            args=['--predefined', 'var1',
                  '--predefined', 'var3'],
        )
        self.assertNotIn('SC2154', output)

    def test_predefined_variables_directive(self) -> None:
        """Test specifying predefined variables in a directive."""

        _, _, output = self._run(
            files={
                'main.yml': [
                    '# gitlab-yaml-shellcheck: predefined=var1',
                    '# gitlab-yaml-shellcheck: predefined=var3',
                    'job:',
                    '  script:',
                    '    - var2=$var1',
                    '    - echo "$var2"',
                    '    - echo "$var3"',
                ],
            },
        )
        self.assertNotIn('SC2154', output)

    def test_var_redefinition(self) -> None:
        """Test the redefinition of variables."""

        _, _, output = self._run(
            files={
                'main.yml': [
                    'variables:',
                    '  var: baz',
                    'job:',
                    '  script:',
                    '    - var=bar',
                    '    - echo "$var"',
                    '  after_script:',
                    '    - echo "$var"',
                ],
            },
        )
        self.assertNotIn('SC2030', output)
        self.assertNotIn('SC2031', output)

    def test_set_e(self) -> None:
        """Test that set -e is set."""

        _, _, output = self._run(
            files={
                'main.yml': [
                    'variables:',
                    '  var: baz',
                    'job:',
                    '  script:',
                    '    - cd /tmp',
                ],
            },
        )
        self.assertNotIn('SC2164', output)

    def test_cache_invalidate_line_numbers(self) -> None:
        """Test the cache invalidation with changing line numbers."""
        script = ['job:',
                  '  script:',
                  '    - unused=1']
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', pathlib.Path(directory)):
            code, messages, _ = self._run(files={'main.yml': script}, cache=True)
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
            code, messages, _ = self._run(files={'main.yml': [''] + script}, cache=True)
            self._assert_messages(code, messages, [
                ('main.yml', 4, 'SC2034'),
            ])
        self.assertEqual(len(run.mock_calls), 2)

    def test_cache_invalidate_options(self) -> None:
        """Test the cache invalidation with shellcheck command line options."""
        script = ['job:',
                  '  script:',
                  '    - unused=1']
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', pathlib.Path(directory)):
            code, messages, _ = self._run(files={'main.yml': script}, cache=True,
                                          args=('--exclude=SC2154',))
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
            code, messages, _ = self._run(files={'main.yml': script}, cache=True)
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
        self.assertEqual(len(run.mock_calls), 2)

    def test_cache_invalidate_check_sourced(self) -> None:
        """Test the cache invalidation when reporting on external scripts."""
        script = ['job:',
                  '  script:',
                  '    - unused=1']
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', pathlib.Path(directory)):
            code, messages, _ = self._run(files={'main.yml': script}, cache=True,
                                          args=('--check-sourced',))
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
            code, messages, _ = self._run(files={'main.yml': script}, cache=True,
                                          args=('--check-sourced',))
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
        self.assertEqual(len(run.mock_calls), 2)

    def test_cache(self) -> None:
        """Test the caching of results."""
        script = ['job:',
                  '  script:',
                  '    - unused=1',
                  'job2:',
                  '  extends: job']
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', pathlib.Path(directory)):
            code, messages, _ = self._run(files={'main.yml': script}, cache=True)
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
            code, messages, _ = self._run(files={'main.yml': script}, cache=True)
            self._assert_messages(code, messages, [
                ('main.yml', 3, 'SC2034'),
            ])
        self.assertEqual(len(run.mock_calls), 1)

    def test_cache_depth(self) -> None:
        """Test the cache cleaning."""
        script = ['job:',
                  '  script:',
                  '    - unused=1']
        with tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', pathlib.Path(directory)),\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_depth', 5):
            for count in range(10):
                self._run(files={'main.yml': ([''] * count) + script}, cache=True)
            self.assertEqual(len(list(pathlib.Path(directory).iterdir())), 5)

    def test_check_sourced(self) -> None:
        """Test the --check-sourced parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                files={
                    'main.yml': [
                        'job:',
                        '  script:',
                        '    - unused=1\n',
                    ],
                },
                args=['--check-sourced']
            )
        self.assertIn('-ax', run.mock_calls[0].args[0])

    def test_check_sourced_missing(self) -> None:
        """Test a missing --check-sourced parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                files={
                    'main.yml': [
                        'job:',
                        '  script:',
                        '    - unused=1',
                    ],
                },
            )
        self.assertIn('-x', run.mock_calls[0].args[0])

    def test_include(self) -> None:
        """Test the --include parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                files={
                    'main.yml': [
                        'job:',
                        '  script:',
                        '    - unused=1\n',
                    ],
                },
                args=['--include', 'SC2034']
            )
        self.assertIn('--include=SC2034', run.mock_calls[0].args[0])

    def test_exclude(self) -> None:
        """Test the --exclude parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                files={
                    'main.yml': [
                        'job:',
                        '  script:',
                        '    - unused=1\n',
                    ],
                },
                args=['--exclude', 'SC2034']
            )
        self.assertIn('--exclude=SC2034', run.mock_calls[0].args[0])

    def test_block_style(self) -> None:
        """Test the line number mapping for block style."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'job:',
                    '  script:',
                    '    - |',
                    '      echo hi',
                    '      unused=1',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main.yml', 5, 'SC2034'),
        ])

    def test_recursive_lists(self) -> None:
        """Test recursive lists."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    '.script: &script',
                    '  - echo hi',
                    '  - unused=1',
                    '',
                    'job:',
                    '  script:',
                    '    - *script',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
        ])

    def test_includes_string(self) -> None:
        """Test include string support."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include: include.yml',
                ],
                'include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            }
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_includes_dict(self) -> None:
        """Test include dict support."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  local: include.yml',
                ],
                'include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_includes_array(self) -> None:
        """Test include array support."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - include.yml',
                ],
                'include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_includes_local_list(self) -> None:
        """Test include local list support."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: include.yml',
                ],
                'include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_includes_other(self) -> None:
        """Test other includes fail."""

        self.assertRaises(Exception, lambda: self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - remote: https://example.com/include.yml',
                ],
                'include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        ))

    def test_includes_wildcards(self) -> None:
        """Test include wildcard support."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: includes/*.yml',
                ],
                'includes/include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_include_missing(self) -> None:
        """Test that a missing include raises an exception."""

        self.assertRaises(Exception, lambda: self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: include.yml',
                    'job:',
                    '  script:',
                    '    - echo foo',
                ],
            },
        ))

    def test_includes_wildcards_recursive(self) -> None:
        """Test include deep wildcard support."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: includes/**/*.yml',
                ],
                'includes/a/b/c/include.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_reference_list(self) -> None:
        """Test referencing a list."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: include.yml',
                    'job:',
                    '  script: !reference [.script]',
                ],
                'include.yml': [
                    '.script:',
                    '  - unused=1',
                    '  - |',
                    '    echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 2, 'SC2034'),
        ])

    def test_reference_list_in_list(self) -> None:
        """Test referencing a list in list context."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: include.yml',
                    'job:',
                    '  script:',
                    '    - !reference [.script]',
                ],
                'include.yml': [
                    '.script:',
                    '  - unused=1',
                    '  - |',
                    '    echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 2, 'SC2034'),
        ])

    def test_reference_string(self) -> None:
        """Test referencing a string."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: include.yml',
                    'job:',
                    '  script:',
                    '    - !reference [.script]',
                ],
                'include.yml': [
                    '.script: |',
                    '  unused=1',
                    '  echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 2, 'SC2034'),
        ])

    def test_reference_dict(self) -> None:
        """Test referencing a dict."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: include.yml',
                    'job: !reference [.job]',
                ],
                'include.yml': [
                    '.job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('include.yml', 3, 'SC2034'),
        ])

    def test_data_file(self) -> None:
        """Test reading data from a file."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
        ])

    def test_data_stdin(self) -> None:
        """Test reading data from stdin."""

        code, messages, _ = self._run(
            stdin=[
                'job:',
                '  script:',
                '    - unused=1',
                '    - |',
                '      echo hi',
            ],
        )
        self._assert_messages(code, messages, [
            ('<stdin>', 3, 'SC2034'),
        ])

    def test_data_stdin_named(self) -> None:
        """Test reading named data from stdin."""

        code, messages, _ = self._run(
            stdin=[
                'job:',
                '  script:',
                '    - unused=1',
                '    - |',
                '      echo hi',
            ],
            args=['--stdin-filename', '{directory}/main.yml'],
        )
        self._assert_messages(code, messages, [
            ('/main.yml', 3, 'SC2034'),  # there is a temp directory before the slash
        ])

    def test_data_file_main(self) -> None:
        """Test reading data from a main file."""

        code, messages, _ = self._run(
            files={
                'main-real.yml': [
                    'include:',
                    '  - local: main.yml',
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
                'main.yml': [
                    '# gitlab-yaml-shellcheck: main=main-real.yml',
                    '{}',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('main-real.yml', 5, 'SC2034'),
        ])

    def test_data_file_main_characters(self) -> None:
        """Test reading data from a main file with some non-word characters."""

        code, messages, _ = self._run(
            files={
                'real/-main_.yml': [
                    'include:',
                    '  - local: ../main.yml',
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
                'main.yml': [
                    '# gitlab-yaml-shellcheck: main=real/-main_.yml',
                    '{}',
                ],
            },
        )
        self._assert_messages(code, messages, [
            ('real/-main_.yml', 5, 'SC2034'),
        ])

    def test_data_stdin_named_main(self) -> None:
        """Test reading named data from a main file via stdin."""

        code, messages, _ = self._run(
            files={
                'main-real.yml': [
                    'include:',
                    '  - local: main.yml',
                    'job:',
                    '  script: !reference [.inline]',
                ],
                'main.yml': [
                    '{ This is invalid YAML as this file should not be used',
                ],
            },
            stdin=[
                '# gitlab-yaml-shellcheck: main=main-real.yml',
                '.inline: |',
                '  unused=1',
                '  echo hi',
            ],
            args=['--stdin-filename', '{directory}/main.yml'],
        )
        self._assert_messages(code, messages, [
            ('main.yml', 3, 'SC2034'),
        ])

    def test_data_subdirs(self) -> None:
        """Test reading data from a main file in a different directory."""

        code, messages, _ = self._run(
            files={
                'main.yml': [
                    'include:',
                    '  - local: subdir/include.yml',
                    'job:',
                    '  script:',
                    '    - unused=1',
                    '    - |',
                    '      echo hi',
                ],
                'subdir/include.yml': [
                    '# gitlab-yaml-shellcheck: main=../main.yml',
                    '{}',
                ],
            },
            filename='subdir/include.yml',
        )
        self._assert_messages(code, messages, [
            ('main.yml', 5, 'SC2034'),
        ])
